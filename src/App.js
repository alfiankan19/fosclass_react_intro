import logo from './logo.svg';
import './App.css';
import Home from './komponen/Home';
import Header from './komponen/Header';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Simpan from './komponen/Simpan';
import React, {useState} from 'react';
function App() {

  const [bookmark,setBookmark] = useState([]);

  const handlesaveBookMark = (val) => {
    console.log("chcek");
  }

  return (
    <div>
      <BrowserRouter>
          <Header/>
          <Switch>
             
              <Route path="/simpan" component={Simpan} />
              <Route path="/" component={Home} />
          </Switch>
      </BrowserRouter>
 
    </div>

  );
}

export default App;
