import { Button, Card, CardContent, CardActions, CircularProgress } from '@material-ui/core';
import React, {useState} from 'react';
import LinkIcon from '@material-ui/icons/Link';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
const Home = () =>{

    const [search,setSearch] = useState('');
    const [isLoading,setisLoading] = useState(false);
    const [repo,setRepo] = useState([])
    const [markRepo,setmarkRepo] = useState([]);
    const handleSearch = (val) => {
        setSearch(val.target.value);
        
    }

    const handlegetSearch = async () => {
        const getData = await fetch('https://api.github.com/search/repositories?q='+search);
        const jsonData = await getData.json();
        return jsonData;
    }

    const handleButtonClick = () => {
        setisLoading(true);
        console.log(search);
        handlegetSearch().then(
            json => {
                console.log(json);
                setRepo(json.items);
                setisLoading(false);
            }
        );
    }


    const handleGithub = (url) => {
            document.location.href = url;
    }

    const handleMark = (data) => {
        setmarkRepo([...markRepo,data]);
        console.log(markRepo);
        localStorage.setItem('bookmark',JSON.stringify(markRepo));
    }

    const handleAwal = () => {
            const getLocaLS = localStorage.getItem('bookmark');
            const toJSON = JSON.parse(getLocaLS);
            
            setmarkRepo(toJSON);
            console.log(markRepo);
    }

    const handleHapus = (key) => {
        console.log(key);

        setmarkRepo(markRepo.filter(item => item.url !== key));
        //console.log(deletedData);
        //localStorage.setItem('bookmark',data);
    }

    return(
        <div>
                <div className="sb">
                    <input style={{outline:0,flex:1,fontSize:20}} type="text" onChange={handleSearch} />
                    <Button variant="contained" onClick={handleButtonClick} >Cari</Button>

                </div>

               <center><Button variant="contained" onClick={handleAwal} >MY BOOKMARK</Button></center>


                <div>

                    {repo.length===0?
                    markRepo.map(
                        val => (
                        <Card style={{marginBottom:15}}>
                            <img style={{height:100}}/>
                            <CardContent>
                               {val.url}
                            </CardContent>
                            <CardActions>
                                    <Button onClick={() => {handleGithub(val.url)}}>
                                        <LinkIcon/>
                                    </Button>

                                    <Button onClick={() => {handleHapus(val.url)}}>
                                       <DeleteForeverIcon/>
                                    </Button>
                            </CardActions>
                        </Card>  
                        )
                    )
                    :null}
                    {
                    isLoading?<center><CircularProgress/></center>:
                    repo.map(
                        (val,idx) => (
                            <Card style={{marginBottom:15}}>
                                <img style={{height:100}} src={val.owner.avatar_url} />
                                <CardContent>
                                    {val.name}
                                </CardContent>
                                <CardActions>
                                    <Button onClick={() => {handleGithub(val.clone_url)}}>
                                        <LinkIcon/>
                                    </Button>
                                    <Button onClick={() => {handleMark({url:val.clone_url,id:idx})}}>
                                       <BookmarkIcon/>
                                    </Button>
                                </CardActions>
                            </Card>
                        )
                    )}
                </div>

        </div>
    )
}

export default Home;