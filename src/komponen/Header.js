import { AppBar, Toolbar } from '@material-ui/core';
import React from 'react';
import './header.css';

const Header = () => {
    return(
        <div style={{marginBottom:100}}>
            <AppBar className="header">
                <Toolbar>
                    <h1>GITFOSS</h1>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default Header;